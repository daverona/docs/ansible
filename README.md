# ansible

## Installation

```bash
# Install ansible using pip
mkdir ansible
python3 -m venv ansible/venv
source ansible/venv/bin/activate
python3 -m pip install --upgrade pip setuptools wheel
python3 -m pip install ansible
deactivate

# Create custom directories
mkdir -p ansible/etc/ansible 
mkdir -p ansible/var/log

# Create ansible config file
cat << EOT | tee ~/.ansible.cfg
[defaults]
interpreter_python = /usr/bin/python3
inventory = $(pwd)/ansible/etc/ansible/hosts
log_path = $(pwd)/ansible/var/log/ansible.log
EOT
```

## Inventory creation

```bash
cat << EOT | tee $(pwd)/ansible/etc/ansible/hosts
all:
  vars:
    ansible_user: supercat  # username
  children:
    cats:  # name of a group
      hosts:
        host1.example:
        host2.example:
        host3.example:
EOT
```

## Key distribution

```bash
# Create ssh key pair
ssh-keygen -b 4096 -t rsa -q -N ""

# Distribute keys to hosts
ssh-copy-id -i ~/.ssh/id_rsa supercat@host1.example
```

## Using ansible

```bash
source ansible/venv/bin/activate
ansible all -m ping
ansible all -m copy -a "src=hello dest=hello"  # create a file
ansible all -m file -a "dest=hello state=absent"  # delete a file
deactivate
```

## References

* [https://docs.ansible.com/ansible/latest/user\_guide/intro\_inventory.html](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html)
